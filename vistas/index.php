
        <div class="texto-encabezado text-xs-center">

            <div class="container">
                <h1 class="display-4 wow bounceIn">Odontología especializada de Ciudad Guzmán</h1>
                <p class="wow bounceIn" data-wow-delay=".3s">Seguridad, etica y responsabilidad</p>
                <a href="contacto" class="btn btn-primary btn-lg">!Haz una cita ahora!</a>
            </div>

        </div>
        <div class="flecha-bajar text-xs-center">
            <a data-scroll href="#agencia"> <i class="fa fa-angle-down" aria-hidden="true"></i></a>
        </div>

    </section>
    <section class="agencia py-1" id="agencia">

        <div class="container">


            <div class="row">

                <div class="col-md-8 col-xl-9 wow bounceIn" data-wow-delay=".3s">
                    <h2 class="h3 text-xs-center text-md-left font-weight-bold">Visítanos</h2>

                    <p>Atención especializada para el cuidado de sus dientes y encías.</p>
                    <p>¿Le gustaría un exámen bucal detallado? <a href="contacto" >!Haz una cita ahora!</a></p>

                    <h2 class="h3 text-xs-center text-md-left">Especialidades</h2>
                    <p>
                        <ul>
                            <li>Cirugía oral</li>
                            <li>Endodoncia</li>
                            <li>Implantología Oral</li>
                            <li>Odontología estética</li>
                            <li>Odontología general</li>
                            <li>Odontología restauradora</li>
                            <li>Odontopediatría</li>
                            <li>Ortodoncia</li>
                            <li>Periodoncia</li>
                            <li>Prostodoncia</li>
                            <li>Implantología Oral</li>
                        </ul>
                    </p>

                    <p>Somos un equipo de cirujanos dentistas  calificados para  procedimientos restaurativos  complejos.</p>
                    <p>Conoce a nuestro <a href="nosotros">equipo de odontólogos</a> comprometidos con tu salud oral.</p>
                    <p>Conoce nuestra <a href="contacto" >ubicación y contacto</a>.</p>

                </div>
                <div class="col-md-4 col-xl-3 wow bounceIn" data-wow-delay=".6s">
                    <img src="<?=APP_PATH?>images/logo_lg.png" alt="La agencia">
                </div>
            </div>
        </div>

    </section>
   
