
        <div class="texto-encabezado text-xs-center">

            <div class="container">
                <h1 class="display-4  wow bounceIn">Servicios</h1>
                <p class="wow bounceIn" data-wow-delay=".3s">Atención especializada para el cuidado de sus dientes y encías</p>

            </div>

        </div>

    </section>
    <section class="ruta py-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-right">
                    <a href="<?=APP_PATH?>">Inicio</a> » Servicios

                </div>
            </div>
        </div>
    </section>
    <main class="py-1 lista-servicios">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <article class="item-servicios">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h4>Implantes dentales</h4>

                        <p>
                            Pequeños dispositivos dentales que se insertan en los maxilares superior e inferior para ayudar a reconstruir una cavidad bucal que tiene pocos o ningún diente que se pueda restaurar.
                        </p>
                        
                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h4>Tratamiento de endodoncias</h4>
                        <p>
                            Tratamiento que consiste en la extracción de la pulpa del diente, un tejido pequeño en forma de hebra, que se encuentra en el centro del conducto del diente. Una vez que la pulpa muerta, se enferma o es dañada, se extrae; el espacio que queda se limpia, se vuelve a dar forma y se rellena.
                        </p>
                        
                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h4>Blanqueamiento dental</h4>
                        <p>
                            En la odontología estética, el blanqueamiento dental es un tratamiento dental estético que logra reducir varios tonos el color original de las piezas dentales, dejando los dientes más blancos y brillantes.
                        </p>
                        
                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h4>Odontología restauradora</h4>
                        <p>
                            La odontología reconstructiva hace referencia a cualquier procedimiento de odontología restauradora que implique el reemplazo o la reparación de los dientes.
                        </p>

                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <i class="fa fa-shield" aria-hidden="true"></i>
                        <h4>Operatoria dental</h4>
                        <p>
                            Rama de la Odontología que se encarga del tratamiento y restauración de los dientes.
                        </p>
                        
                    </article>
                </div>

            </div>
        </div>
    </main>




