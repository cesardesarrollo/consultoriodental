
        <div class="texto-encabezado text-xs-center">

            <div class="container">
                <h1 class="display-4">Nosotros</h1>
                <p class="wow bounceIn" data-wow-delay=".3s">¿Quienes somos? y ¿Que hacemos?.</p>

            </div>

        </div>

    </section>
    <section class="ruta py-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-right">
                    <a href="<?=APP_PATH?>">Inicio</a> » Nosotros

                </div>
            </div>
        </div>
    </section>



    <main class="py-1 lista-servicios">
        <div class="container">



            <div class="row">

                <div class="col-md-8 col-xl-9">
                    <h2 class="h3 text-xs-center text-md-left">Quienes somos</h2>

                        <p>Somos un grupo de profesionales dedicados al cuidado de sus dientes y encías, ofreciendo lo más actualizado en diagnóstico, prevención y tratamiento de enfermedades y alteraciones buco-dentales, todo esto con materiales y técnicas de vanguardia. Nuestra atención es profesional y personalizada, en un ambiente cómodo y agradable.</p>

                </div>
                <div class="col-md-4 col-xl-3 wow bounceIn" data-wow-delay=".6s">
                    
                </div>

            </div>


            <div class="row">
                <div class="col-md-6 col-xl-8">
                    <h2 class="h3 text-xs-center text-md-left">Especialidades</h2>


                    <p>
                        <ul>
                            <li>Cirugía oral</li>
                            <li>Endodoncia</li>
                            <li>Implantología Oral</li>
                            <li>Odontología estética</li>
                            <li>Odontología general</li>
                            <li>Odontología restauradora</li>
                            <li>Odontopediatría</li>
                            <li>Ortodoncia</li>
                            <li>Periodoncia</li>
                            <li>Prostodoncia</li>
                            <li>Implantología Oral</li>
                        </ul>

                    </p>   

                </div>
                <div class="col-md-6 col-xl-4 wow bounceIn" data-wow-delay=".6s">
                    <img src="<?=APP_PATH?>images/logo_lg.png" class="img-fluid m-x-auto" alt="Nosotros">
                </div>


            </div>


            <h2 class="h3 text-xs-center text-md-left">Nuestro equipo</h2>

            <div class="row">

                <div class="col-md-4">
                    <article class="item-servicios">
                        <img src="http://www.odontologiaespecializadacg.com/wp-content/uploads/2015/03/DSC_1036-good-CUT.jpg" class="img-fluid" alt="Asistente dental" />

                        <h4>Dra. Rosina Miranda Sanchez</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Cirujano dentista</li>
                                <li>Odontología estética</li>
                                <li>Odontología general</li>
                                <li>Ortodoncia</li>
                            </ul>
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#rosinamiranda">Más información</a>

                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <img src="http://www.odontologiaespecializadacg.com/wp-content/uploads/2015/03/DSC_1013-good-CUT-e1480558460714.jpg" class="img-fluid" alt="Asistente dental" />

                        <h4>Dra. Alejandra Montiel López</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Cirujano dentista</li>
                                <li>Odontología estética</li>
                                <li>Odontología general</li>
                                <li>Periodoncia</li>
                            </ul>
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#alejandramontiel">Más información</a>

                    </article>
                </div>



                <div class="col-md-4">
                    <article class="item-servicios">
                        <img src="http://www.odontologiaespecializadacg.com/wp-content/uploads/2016/11/DSC_0908-good-CUT-e1480558042595.jpg" class="img-fluid" alt="Asistente dental" />

                        <h4>M. en C. Juan Carlos García Núñez</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>C.D. JUAN CARLOS GARCÍA NÚÑEZ</li>
                                <li>Maestría en Ciencias Médicas</li>
                                <li>Especialista en Prostodoncia</li>
                                <li>Especialista en Periodoncia</li>
                                <li>Posgraduado en Implantología oral</li>
                                <li>Odontología estética</li>
                                <li>Odontología restauradora</li>
                                <li>Profesor en Implantología Universidad de Guadalajara</li>
                            </ul>
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#juancarlos">Más información</a>

                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">

                        <i class="fa fa-bullseye" aria-hidden="true"></i>

                        <h4>
                            Dra. Mónica Alejandra García Núñez
                        </h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Especialista en Endodoncia</li>
                                <li>Endodoncia</li>
                            </ul>
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#monicaalejandra">Más información</a>

                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        
                        <i class="fa fa-bullseye" aria-hidden="true"></i>

                        <h4>Dr. Alfonso García Núñez</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Dr. Cirujano Dentista</li>
                                <li>Odontología general</li>
                                <li>Odontología restauradora</li>
                                <li>Ortodoncia</li>
                            </ul>
                        </p>
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#alfonsogarcia">Más información</a>

                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <img src="http://www.odontologiaespecializadacg.com/wp-content/uploads/2016/12/DSC_0894.jpg" class="img-fluid" alt="Asistente dental" />
                        <h4>Lic. Karen Morales</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Asistente dental</li>
                            </ul>
                        </p>

                    </article>
                </div>


                <div class="col-md-4">
                    <article class="item-servicios">
                        <img src="http://www.odontologiaespecializadacg.com/wp-content/uploads/2016/12/DSC_1046-good-CUT-e1480557792462.jpg" class="img-fluid" alt="Asistente dental" />

                        <h4>L.N. Evelyn Margot Madrigal</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Asistente dental</li>
                            </ul>
                        </p>

                    </article>
                </div>

                <div class="col-md-4">
                    <article class="item-servicios">
                        <img src="http://www.odontologiaespecializadacg.com/wp-content/uploads/2016/12/DSC_1024-good2-CUT.jpg" class="img-fluid" alt="Asistente dental" />

                        <h4>L.N.  Anahí Velazco García</h4>
                        <p>
                            <ul style="text-align: left;">
                                <li>Asistente dental</li>
                            </ul>
                        </p>

                    </article>
                </div>


            </div>


        </div>
    </main>





    <!-- Modal 1  -->
    <div class="modal fade" id="juancarlos" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">M. en C. Juan Carlos García Núñez</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">

                            <h3>Grados y educación</h3>
                            <p>Cirujano dentista, Universidad de Guadalajara, Generación 1990-1994.</p>
                            <p>Especialidad en Prostodoncia, Universidad de Guadalajara, Generación 1998-2000.</p>
                            <p>Especialidad en Periodoncia, Universidad de Guadalajara, Generación 2002-2004.</p>
                            <p>Diplomado en Implantología Oral, Universidad Nacional Autónoma de México, generación 2012-2013.</p>
                            <p>Maestría en Ciencias Médicas, Universidad de Colima, generación 2014-2016.</p>
                             
                            <h3>Docencia</h3>
                            <p>Profesor e instructor de Implantología Oral, de los programas de especialidad en Periodoncia y Prostodoncia del departamento de Clínicas Odontológicas Integrales de la Universidad de Guadalajara.</p>

                            <h3>Practica Privada</h3>
                            <p>Experiencia laboral  desde 1995 a la fecha.</p>

                            <h3>Contacto</h3>
                            <p><i>cdjuancarlos_1@hotmail.com</i></p>

                        </div>

                        <div class="col-md-4">
                            <img src="<?=APP_PATH?>images/logo_lg.png" alt="" width="200" class="img-fluid m-x-auto">
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>


    <!-- Modal 2  -->
    <div class="modal fade" id="monicaalejandra" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Dra. Mónica Alejandra García Núñez</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">

                            <h3>Grados y educación</h3>
                            <p>Cirujano dentista, Universidad de Guadlajara, Generacion 1994-1998.</p>
                            <p>Especialidad en Endodoncia, Universidad de Guadalajara, Generación 2002-2004.</p>
                            <p>Curso Propedéutico a doctorado en inmunología.</p>

                        </div>

                        <div class="col-md-4">
                            <img src="<?=APP_PATH?>images/logo_lg.png" alt="" width="200" class="img-fluid m-x-auto">
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>



    <!-- Modal 3  -->
    <div class="modal fade" id="rosinamiranda" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Dra. Mónica Alejandra García Núñez</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">

                            <h3>Grados y educación</h3>
                            <p>Cirujano Dentista</p>
                            <p>Residente Especialidad en Ortodoncia</p>
                            <h3>Contacto</h3>
                            <p><i>cdrosinamiranda@odontologiaespecializadacg.com</i></p>

                        </div>

                        <div class="col-md-4">
                            <img src="<?=APP_PATH?>images/logo_lg.png" alt="" width="200" class="img-fluid m-x-auto">
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>



    <!-- Modal 4 -->
    <div class="modal fade" id="alejandramontiel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Dra. Mónica Alejandra García Núñez</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">

                            <h3>Grados y educación</h3>
                            <p>Cirujano Dentista</p>
                            <h3>Contacto</h3>
                            <p><i>cdmonicaalejandra@odontologiaespecializadacg.com</i></p>

                        </div>

                        <div class="col-md-4">
                            <img src="<?=APP_PATH?>images/logo_lg.png" alt="" width="200" class="img-fluid m-x-auto">
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>



    <!-- Modal 5 -->
    <div class="modal fade" id="alfonsogarcia" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Dr. Alfonso García Núñez</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-8">

                            <h3>Grados y educación</h3>
                            <p>Cirujano dentista, universidad de Guadalajara, Generación 1985-1989.</p>
                            <p>Ortodoncia, Odontología restauradora y general.</p>
                            <p>Miembro de la asociación dental mexicana.</p>

                        </div>

                        <div class="col-md-4">
                            <img src="<?=APP_PATH?>images/logo_lg.png" alt="" width="200" class="img-fluid m-x-auto">
                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>


