

<?php

    // Envío de email a cliente
    if(isset($_POST['enviar_cita'])){

        $nombre = (isset($_POST['nombre'])) ? $_POST['nombre'] : "";
        $email = (isset($_POST['email'])) ? $_POST['email'] : "";
        $telefono = (isset($_POST['telefono'])) ? $_POST['telefono'] : "";
        $mensaje = (isset($_POST['mensaje'])) ? $_POST['mensaje'] : "";
        $fecha_cita = (isset($_POST['fecha_cita'])) ? $_POST['fecha_cita'] : "";


        //$para      = 'cdjuancarlos_1@hotmail.com';
        $para      = 'funeralelectrico@hotmail.com';
        $titulo    = 'Cita desde sitio web';
        $body = "<html><body> <p>Cita desde sitio web.</p> 
                <p>Nombre: ".$nombre."</p>
                <p>Fecha pidiendo cita: $fecha_cita</p>
                <p>Email: $email</p>
                <p>Teléfono: $telefono</p>
                <p>Mensaje: ".$mensaje."</p>
                 </body></html>";


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: usuarioscita@odontologiaespecializadacg.com' . "\r\n";


        mail($para, $titulo, $body, $headers);

        Core::alert("Tu petición de cita ha sido enviada exitosamente.");

    }


    // Envío de email a cliente
    if(isset($_POST['enviar_mensaje'])){

        $nombre = (isset($_POST['nombre'])) ? $_POST['nombre'] : "";
        $email = (isset($_POST['email'])) ? $_POST['email'] : "";
        $telefono = (isset($_POST['telefono'])) ? $_POST['telefono'] : "";
        $mensaje = (isset($_POST['mensaje'])) ? $_POST['mensaje'] : "";


        //$para      = 'cdjuancarlos_1@hotmail.com';
        $para      = 'funeralelectrico@hotmail.com';
        $titulo    = 'Mensaje desde sitio web';

        $body = "<html><body> <p>Cita desde sitio web.</p> 
                <p>Nombre: ".$nombre."</p>
                <p>Email: $email</p>
                <p>Teléfono: $telefono</p>
                <p>Mensaje: ".$mensaje."</p>
                 </body></html>";


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: usuariosmensaje@odontologiaespecializadacg.com' . "\r\n";


        mail($para, $titulo, $body,$headers);

        Core::alert("Tu mensaje ha sido enviado exitosamente.");

    }

?>

        <div class="texto-encabezado text-xs-center">

            <div class="container">
                <h1 class="display-4 wow bounceIn">Contacto y ubicación</h1>
                <p class="wow bounceIn" data-wow-delay=".3s">¿Le gustaría un exámen bucal detallado? Haga una cita ahora mismo!</p>
            </div>

        </div>

    </section>
    <section class="ruta py-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-right">
                    <a href="<?=APP_PATH?>">Inicio</a> » Contacto y ubicación

                </div>
            </div>
        </div>
    </section>
    <main class="py-1">
        <div class="container">


            <div class="row">

                <div class="col-md-12">

                    <h1>Contáctanos</h1>
                    <p>Visítanos ó llena uno de los formularios y entraremos en contacto contigo lo mas pronto posible.</p>

                </div>
            </div>

            <div class="row">

                <div class="col-md-7">

                    <h2 class="m-b-2">Déjanos un mensaje</h2>
                    <form action="#" method="post">

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label">Nombre</label>

                            <div class="col-md-8">
                                <input class="form-control" type="text" id="nombre" name="nombre" placeholder="Ingrese su nombre" data-toggle="tooltip" data-placement="top" title="Ingrese su nombre completo">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">Email</label>

                            <div class="col-md-8">
                                <input class="form-control" type="email" id="email" name="email" placeholder="Ingrese su email" data-toggle="tooltip" data-placement="top" title="Ingrese su email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="teléfono" class="col-md-4 col-form-label">Teléono ó WhatsApp</label>

                            <div class="col-md-8">
                                <input class="form-control" type="text" id="telefono" name="telefono" placeholder="Ingrese su teléfono ó WhatsApp" data-toggle="tooltip" data-placement="top" title="Ingrese su teléfono ó WhatsApp">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="mensaje" class="col-md-4 col-form-label">Mensaje</label>

                            <div class="col-md-8">
                                <textarea class="form-control" rows="5" id="mensaje" name="mensaje" placeholder="Ingrese su mensaje" data-toggle="tooltip" data-placement="top" title="Ingrese un mensaje"></textarea>

                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <input name="enviar_mensaje" type="submit" class="btn btn-primary" value="Enviar">
                                <button type="reset" class="btn btn-secondary">Limpiar</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-md-5">

                    <h2 class="m-b-2">Realiza una cita</h2>
                    <p>Recuerda que llamando a nuestros teléfonos tendrás una atención mas rápida y completa, intentalo antes de llenar el siguiente formulario.</p>
                    <form action="#" method="post">

                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label">Nombre</label>

                            <div class="col-md-8">
                                <input class="form-control" type="text" id="nombre" name="nombre" placeholder="Ingrese su nombre" data-toggle="tooltip" data-placement="top" title="Ingrese su nombre completo">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label">Email</label>

                            <div class="col-md-8">
                                <input class="form-control" type="email" id="email" name="email" placeholder="Ingrese su email" data-toggle="tooltip" data-placement="top" title="Ingrese su email">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="teléfono" class="col-md-4 col-form-label">Teléono ó WhatsApp</label>

                            <div class="col-md-8">
                                <input class="form-control" type="text" id="telefono" name="telefono" placeholder="Ingrese su teléfono ó WhatsApp" data-toggle="tooltip" data-placement="top" title="Ingrese su teléfono ó WhatsApp">
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="teléfono" class="col-md-4 col-form-label">Fecha que deseas visitarnos</label>

                            <div class="col-md-8">
                                <input class="form-control" type="text" id="fecha_cita" name="fecha_cita" placeholder="Fecha tentativa a visitarnos" data-toggle="tooltip" data-placement="top" title="Fecha tentativa a visitarnos">
                            
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="mensaje" class="col-md-4 col-form-label">Describe brevemente tu problema</label>

                            <div class="col-md-8">
                                <textarea class="form-control" rows="5" id="mensaje" name="mensaje" placeholder="Ingrese su mensaje" data-toggle="tooltip" data-placement="top" title="Ingrese un mensaje"></textarea>

                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <input name="enviar_cita" type="submit" class="btn btn-primary" value="Enviar">
                                <button type="reset" class="btn btn-secondary">Limpiar</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
            
            <div class="row">
                <div class="col-md-8">
                    <h2 class="m-b-2">Ubicación</h2>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d693.8662637182067!2d-103.4605219644973!3d19.716046735727875!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xda018a176534ce29!2sODONTOLOGIA+ESPECIALIZADA+DE+CIUDAD+GUZMAN!5e0!3m2!1ses-419!2smx!4v1480319529993" style="width:100vh; height: 50vh;" class="img-fluid" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div class="col-md-4">
                    <h2 class="m-b-2">&nbsp;</h2>
                    <h4>Contactanos por</h4>
                    <p>Teléfono citas: (341) 412-86-46 
                    <p>WhatsApp: 341-1512-342</p>
                    <p>Email: cdjuancarlos_1@hotmail.com</p>
                    <h4>Visítanos en</h4>
                    <p>Dirección: Cristóbal Colón #568,</p>
                    <p>Ciudad Guzmán Jalisco, México.</p>
                    <p>Horario: Lunes a Sabado - 10:00 am. a 2:00 pm. y de 4:00 pm. a 8:00 pm.</p>

                </div>
            </div>

        </div>
    </main>

    <script>
        $(function(){

            $("#fecha_cita").datepicker({ format: 'yyyy-mm-dd' });

        });
    </script>



